# README #

[You Need A Budget](http://www.youneedabudget.com/) is a decent budgetting application. Banks do not supply transaction files in acceptable formats, requiring conversion. You Need A Budget *Converter* allows the conversion of transaction files to [a format that YNAB accepts](http://www.youneedabudget.com/support/article/csv-file-importing).

ynabc is not affiliated with the YNAB company.

[DOWNLOAD NOW](https://bitbucket.org/xsysstar/ynabc/downloads/ynabc.jar)

### Features ###
ynabc supports:

* Importing Rabobank CSV-files.
* Importing PayPal CSV-files ("*Comma separated file - Payments that affect the balance*") including foreign-currency conversions. *[Since v0.2.0]*
* Importing Robeco stock portfolio performance statistics (`https://www.robeco.nl/ofs-web/transactionsAndAccountPerformance?graphType=VALUE_SINCE_START&dataInterval=DAY&accountId=0&_=*YOUR_ACCOUNT_NUMBER*`). *[Since v0.3.3]*
* Exporting YNAB-compatible CSV-files.
* Separating transactions into one output file per account.
* Intelligent line joining of transaction memos.
* Searching for importable files.


### Usage ###
ynabc is a command line application that takes paths to importable files (or paths to directories that may contain them) as its arguments. Without arguments, it will convert files in the working directory. Exported files are saved in the directory the (first) import file originated.

Example for Rabobank-files:

```
#!sh

java -jar /path/to/ynabc.jar /path/to/rabobank/transactions.txt
```

or simply

```
#!sh

java -jar /path/to/ynabc.jar
```
to search in the working directory for `transactions.txt`.

### Contributions ###
Feel free to contribute to ynabc in any way you wish: a GUI, integration with YNAB, file dropping daemons etc. Of course, ynabc can use support for new import or export formats too.

#### Your bank transaction format not supported? ####
If you cannot program yourself, you can still request support for your bank's format; just create a new issue. We will need from you: 

* an official specification of the file format (if available) and;
* a file with actual transactions for testing purposes—you may/should obfuscate the file data, but please do not change lengths or structure of fields. (Retain the original unobfuscated file as well, just in case.)

Alternatively, you may program a new format yourself and submit a pull-request. ynabc is small enough for hobby programmers to adapt it.