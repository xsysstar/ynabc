package eu.imperamus.ynabc.conversion.paypal;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.conversion.currency.CurrencyConvertedImporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.CompositeAccount;
import eu.imperamus.ynabc.domain.SimpleAccount;

public class PaypalImporterTest {
	private ModelImporter importer = new CurrencyConvertedImporter(
			new PaypalCsvImporter(), NumberFormat.getCurrencyInstance()
					.getCurrency());

	private Map<String, Account> subAccounts;
	private Account totalAccount;

	@Before
	public void setUp() throws Exception {
		subAccounts = new TreeMap<String, Account>();
		totalAccount = new CompositeAccount(subAccounts);
	}

	@Test
	public void test() throws IOException, ParseException {
		// Given
		File file = new File(getClass().getResource("Downloaden.csv").getPath().replace("%20", " "));
		importer.importModel(file, totalAccount);

		// Then
		Assert.assertEquals("number of sub-accounts", 1, subAccounts.size());
		Assert.assertEquals("number of transactions", 10, subAccounts.values()
				.iterator().next().getTransactions().size());
		Assert.assertEquals("account balance", -116.14,
				((SimpleAccount) subAccounts.values().iterator().next())
						.getBalance(), 0.01);
	}

}
