package eu.imperamus.ynabc;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import eu.imperamus.ynabc.conversion.CompositeImporter;
import eu.imperamus.ynabc.conversion.ModelExporter;
import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.conversion.currency.CurrencyConvertedImporter;
import eu.imperamus.ynabc.conversion.paypal.PaypalCsvImporter;
import eu.imperamus.ynabc.conversion.rabobank.RabobankCsvImporter;
import eu.imperamus.ynabc.conversion.ynab.YnabCsvExporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.CompositeAccount;

/**
 * Hello world!
 *
 */
@Data
@RequiredArgsConstructor
public class Convertor {
	private final ModelImporter importer;
	private final ModelExporter exporter;

	private final Map<String, Account> subAccounts;
	private final Account totalAccount;

	public Convertor(Map<String, Account> subAccounts) {
		this( // importer
				new CurrencyConvertedImporter(new CompositeImporter(
						new RabobankCsvImporter(), new PaypalCsvImporter()),
						NumberFormat.getCurrencyInstance().getCurrency()),
				// exporter
				new YnabCsvExporter(),
				// subaccounts
				subAccounts,
				// TotalAccount
				new CompositeAccount(subAccounts));
	}

	public Convertor() {
		this(new TreeMap<>());
	}

	public static void main(String[] args) throws IOException, ParseException {
		if (args == null || args.length == 0)
			args = new String[] { "." };

		System.out.println(String.format(
				"You Need A Budget Convertor (version %s).", Convertor.class
						.getPackage().getImplementationVersion()));

		Convertor c = new Convertor();

		val files = findFiles(c.getImporter().getFileFilter(), args);
		File outputDirectory = files.isEmpty() ? new File(".") : files
				.iterator().next().getParentFile();

		System.out.println();
		c.importTransactionsFromFiles(files);
		System.out.println();
		c.exportAccountsToFiles(outputDirectory);
		System.out.println();
		System.out.println("You Need A Budget Convertor no longer.");
	}

	private void exportAccountsToFiles(File outputDirectory)
			throws FileNotFoundException, IOException {
		System.out.println("Exporting accounts to "
				+ outputDirectory.getAbsolutePath());
		for (Entry<String, Account> e : subAccounts.entrySet()) {
			String identifier = e.getKey();
			Account account = e.getValue();

			val outputFileName = identifier
					+ " - "
					+ new SimpleDateFormat("yyyy-MM-dd").format(account
							.getTransactions().get(0).getDate())
					+ YnabCsvExporter.EXPORT_FILE_EXTENSION;
			System.out.println("Exporting " + outputFileName + " ("
					+ account.getTransactions().size() + " transactions)...");
			val outputStream = new FileOutputStream(new File(outputDirectory,
					outputFileName));
			exporter.exportModel(outputStream, account);

		}
	}

	private void importTransactionsFromFiles(Iterable<File> files)
			throws IOException, ParseException {
		int oldSize = totalAccount.getTransactions().size();

		for (File file : files) {
			System.out.println("Importing file " + file.getAbsolutePath());

			importer.importModel(file, totalAccount);
		}

		int newSize = totalAccount.getTransactions().size();
		System.out.println(String.format("Imported %d transaction(s).", newSize
				- oldSize));
	}

	private static Collection<File> findFiles(FileFilter fileFilter,
			String... fileNames) {
		List<File> result = new LinkedList<>();

		for (String fileName : fileNames) {
			File file = new File(fileName);
			if (file.isDirectory()) {
				File[] subFiles = file.listFiles(fileFilter);
				if (subFiles != null) {
					for (File subFile : subFiles) {
						if (!subFile.getName().endsWith(
								YnabCsvExporter.EXPORT_FILE_EXTENSION))
							result.add(subFile);
					}
				}
			} else {
				result.add(file);
			}
		}

		return result;
	}
}
