package eu.imperamus.ynabc.conversion;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.ParseException;

import eu.imperamus.ynabc.domain.Account;

public interface ModelImporter {

	public FileFilter getFileFilter();

	public void importModel(File file, Account model) throws IOException,
			ParseException;

}
