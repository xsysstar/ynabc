package eu.imperamus.ynabc.conversion.currency;

import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.Transaction;

@RequiredArgsConstructor
@Deprecated
public class CurrencyConverted implements Account {
	private final @NonNull @Getter Account inner;
	private final @NonNull @Getter Currency targetCurrency;

	private final NavigableSet<Transaction> foreignTransactions = new TreeSet<>();

	@Override
	public List<Transaction> getTransactions() {
		/*
		 * if (!foreignTransactions.isEmpty()) throw new
		 * IllegalStateException("There are " + foreignTransactions.size() +
		 * " unconverted transactions.");
		 */
		flushUnconvertedTransactions();
		return inner.getTransactions();
	}

	public void flushUnconvertedTransactions() {
		for (Transaction foreignTransaction : foreignTransactions) {
			foreignTransaction
					.setMemo(getConvertedTransactionMemo(foreignTransaction));
			foreignTransaction.setFlow(0);
			foreignTransaction.setCurrency(null);
			inner.addTransaction(foreignTransaction);
		}
		foreignTransactions.clear();
	}

	@Override
	public void addTransaction(Transaction transaction) {
		if (!transaction.getCurrency().equals(targetCurrency)) {
			foreignTransactions.add(transaction);
		} else {
			// Convert backlog of foreign transactions
			Iterator<Transaction> itor = foreignTransactions.iterator();
			while (itor.hasNext()) {
				// Is the current transaction simultaneous to the foreign
				// transactions?
				Transaction foreignTransaction = itor.next();

				if (foreignTransaction.compareTo(transaction) == 0) {
					foreignTransaction
							.setMemo(getConvertedTransactionMemo(foreignTransaction));
					foreignTransaction.setFlow(-transaction.getFlow());
					foreignTransaction.setCurrency(targetCurrency);

					inner.addTransaction(foreignTransaction);
					itor.remove();
				}
			}

			// Finally add new transaction
			inner.addTransaction(transaction);
		}
	}

	private String getConvertedTransactionMemo(Transaction foreignTransaction) {
		return foreignTransaction.getMemo() + " (= "
				+ foreignTransaction.getCurrency() + " "
				+ foreignTransaction.getFlow() + ")";
	}

	@Override
	public void removeTransaction(Transaction transaction) {
		// TODO Auto-generated method stub

	}
}
