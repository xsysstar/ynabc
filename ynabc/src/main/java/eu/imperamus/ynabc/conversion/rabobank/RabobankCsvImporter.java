package eu.imperamus.ynabc.conversion.rabobank;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.val;
import au.com.bytecode.opencsv.CSVReader;
import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.Transaction;

/**
 * @author Wanno Drijfhout
 * @see <a
 *      href="https://www.rabobank.nl/bedrijven/betalen/zakelijk-internetbankieren/internetbankieren-professional/support/bestanden/#verant">Official
 *      CSV-file documentation (Dutch)</a>
 */
public class RabobankCsvImporter implements ModelImporter {

	@Getter
	private Pattern typicalFileNamePattern = Pattern
			.compile("^CSV_A_\\d+_\\d+.csv$");

	@Getter
	private FileFilter fileFilter = pathname -> typicalFileNamePattern.matcher(
			pathname.getName()).matches();

	@Override
	public void importModel(File file, Account model) throws IOException,
			ParseException {
		try (CSVReader reader = new CSVReader(new FileReader(file))) {
			String[] nextLine;
			// Skip header
			reader.readNext();
			while ((nextLine = reader.readNext()) != null
					&& nextLine.length > 1) {
				val transaction = parseTransaction(nextLine);
				model.addTransaction(transaction);
			}
		}
	}

	protected Transaction parseTransaction(String[] fields)
			throws ParseException {
		Transaction transaction = new Transaction();
		if (fields.length != 26)
			throw new ParseException("Line does not have 26 fields.", 0);

		// Note that fields are numbered (in official documentation and comments
		// below) ONE-based.
		//"IBAN/BBAN","Munt","BIC","Volgnr","Datum","Rentedatum","Bedrag","Saldo na trn","Tegenrekening IBAN/BBAN","Naam tegenpartij","Naam uiteindelijke partij","Naam initiërende partij","BIC tegenpartij","Code","Batch ID","Transactiereferentie","Machtigingskenmerk","Incassant ID","Betalingskenmerk","Omschrijving-1","Omschrijving-2","Omschrijving-3","Reden retour","Oorspr bedrag","Oorspr munt","Koers"

		// 1 (rekeningnummer)
		transaction.setAccountNumber(fields[0]);

		// 2 (muntsoort)
		transaction.setCurrency(Currency.getInstance(fields[1]));

		// 5 (datum)
		transaction.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(fields[4]));

		// 7 (bedrag)
		val format = new DecimalFormat("+0,00;-0,00");
		double amount = format.parse(fields[6]).doubleValue();
		transaction.setFlow(amount);
		
		// 9 (tegenrekening) ++ 10 (naar_naam) / 11 (naam uiteindelijke partij)
		transaction.setPayeeNumber(fields[8]);
		transaction.setPayeeName(!fields[10].isEmpty() ? fields[10] : fields[9]);

		// No category
		transaction.setCategory("");

		// 20-22 (omschr1-3)
		transaction.setMemo(smartStringJoin(Arrays.copyOfRange(fields,
				19, 21)));

		return transaction;
	}

	static String smartStringJoin(String[] parts) {
		StringBuilder sb = new StringBuilder();
		for (String part : parts) {
			sb.append(part.trim());
			if (part.length() < 35)
				sb.append(" ");
		}
		return sb.toString().trim();
	}
	
	@Override
	public String toString() {
		return "Rabobank CSV";
	}
}
