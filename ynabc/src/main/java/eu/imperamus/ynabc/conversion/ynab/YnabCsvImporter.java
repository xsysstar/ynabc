package eu.imperamus.ynabc.conversion.ynab;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.val;
import au.com.bytecode.opencsv.CSVReader;
import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.Transaction;

public class YnabCsvImporter extends YnabFormat implements ModelImporter {

	/**
	 * Typical file name pattern
	 *
	 * @see {@link YnabFORMAT#EXPORT_FILE_EXTENSION}
	 */
	@Getter
	private Pattern typicalFileNamePattern = Pattern
			.compile("^(.*)\\.ynabc\\.csv$");

	@Getter
	private FileFilter fileFilter = pathname -> typicalFileNamePattern.matcher(
			pathname.getName()).matches();

	@Override
	public void importModel(File file, Account model) throws IOException,
			ParseException {
		try (CSVReader reader = new CSVReader(new FileReader(file))) {
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null
					&& nextLine.length > 1) {
				val transaction = parseTransaction(nextLine);
				model.addTransaction(transaction);
			}
		}
	}

	protected Transaction parseTransaction(String[] fields)
			throws ParseException {
		Transaction transaction = new Transaction();
		if (fields.length != 7)
			throw new ParseException("Line does not have 7 fields.", 0);

		transaction.setDate(DATE_FORMAT.parse(fields[0]));

		transaction.setCheckNumber(fields[1]);

		transaction.setPayeeName(unsmartPayeeName(fields[2]));
		transaction.setPayeeNumber(unsmartPayeeNumber(fields[2]));

		transaction.setCategory(fields[3]);

		transaction.setMemo(fields[4]);

		double outflow = NUMBER_FORMAT.parse(fields[5]).doubleValue();
		double inflow = NUMBER_FORMAT.parse(fields[6]).doubleValue();
		transaction.setFlow(inflow - outflow);

		return transaction;
	}
	
	@Override
	public String toString() {
		return "YNAB CSV";
	}
}
