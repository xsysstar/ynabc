package eu.imperamus.ynabc.conversion.ynab;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class YnabFormat {
	public static final String EXPORT_FILE_EXTENSION = ".ynabc.csv";

	public static final String[] HEADER = new String[] {//
	"Date", "Check", "Payee", "Category", "Memo", "Outflow", "Inflow" };

	public final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
	public static final NumberFormat NUMBER_FORMAT = NumberFormat
			.getInstance(Locale.US);
	{
		NUMBER_FORMAT.setGroupingUsed(false);
	}

	public static String smartPayee(String payeeName, String payeeNumber) {
		if (payeeName == null || payeeName.isEmpty())
			return "[" + payeeNumber + "]";
		else if (payeeNumber == null || payeeNumber.isEmpty())
			return payeeName;
		else
			return payeeName + " [" + payeeNumber + "]";
	}

	public static String unsmartPayeeName(String smartPayee) {
		if (smartPayee == null)
			return null;

		int i = smartPayee.indexOf("[");
		if (i == -1) {
			return smartPayee;
		} else {
			return smartPayee.substring(0, i - 1);
		}
	}

	public static String unsmartPayeeNumber(String smartPayee) {
		if (smartPayee == null)
			return null;

		int iBegin = smartPayee.lastIndexOf("[");
		int iEnd = smartPayee.lastIndexOf("]");

		return smartPayee.substring(iBegin + 1, iEnd - 1);
	}

}
