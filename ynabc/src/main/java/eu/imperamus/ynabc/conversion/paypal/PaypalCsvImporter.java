package eu.imperamus.ynabc.conversion.paypal;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.val;
import au.com.bytecode.opencsv.CSVReader;
import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.Transaction;

/**
 * @author Wanno Drijfhout
 */
public class PaypalCsvImporter implements ModelImporter {

	@Getter
	private Pattern typicalFileNamePattern = Pattern
			.compile("^(Download).*\\.csv$");

	@Getter
	private FileFilter fileFilter = pathname -> typicalFileNamePattern.matcher(
			pathname.getName()).matches();

	@Override
	public void importModel(File file, Account model) throws IOException,
			ParseException {
		try (CSVReader reader = new CSVReader(new FileReader(file))) {
			String[] nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null
					&& nextLine.length > 1) {
				val transaction = parseTransaction(nextLine);
				model.addTransaction(transaction);
			}
		}
	}

	protected Transaction parseTransaction(String[] fields)
			throws ParseException {
		Transaction transaction = new Transaction();
		if (fields.length != 11)
			throw new ParseException("Line does not have 11 fields.", 0);

		// 0 (date) ++ 1 (time) ++ 2 (timezone)
		transaction.setDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
				.parse(fields[0] + " " + fields[1] + " " + fields[2]));
		// transaction
		// .setDate(new SimpleDateFormat("dd-mm-yyyy").parse(fields[0]));

		// 3 (payee name)
		transaction.setPayeeNumber(null);
		transaction.setPayeeName(fields[3]);

		// 4 (type=description)
		transaction.setMemo(fields[4]);

		// 5 (state) -- ignore

		// 6 (currency)
		transaction.setCurrency(Currency.getInstance(fields[6]));

		// 7 (amount)
		double amount = NumberFormat.getNumberInstance().parse(fields[7])
				.doubleValue();
		transaction.setFlow(amount);

		// 8 (receipt)
		transaction.setCheckNumber(fields[8]);

		// 9 (saldo) -- ignore

		// No category
		transaction.setCategory("");

		transaction.setAccountName("PayPal");

		return transaction;
	}

	static String smartStringJoin(String[] parts) {
		StringBuilder sb = new StringBuilder();
		for (String part : parts) {
			sb.append(part);
			if (part.length() < 35)
				sb.append(" ");
		}
		return sb.toString().trim();
	}
	
	@Override
	public String toString() {
		return "PayPal CSV";
	}
}
