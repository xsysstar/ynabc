package eu.imperamus.ynabc.conversion;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import eu.imperamus.ynabc.domain.Account;

public class CompositeImporter implements ModelImporter {
	private final List<ModelImporter> importers = new LinkedList<>();

	public CompositeImporter(ModelImporter... importers) {
		this.importers.addAll(Arrays.asList(importers));
	}

	public List<ModelImporter> importers() {
		return importers;
	}

	@Getter
	private FileFilter fileFilter = new MultiFileFilter();

	@Override
	public void importModel(File file, Account model) throws IOException,
			ParseException {

		// Just try any of the available importers
		boolean success = false;
		for (ModelImporter modelImporter : importers) {
			try {
				modelImporter.importModel(file, model);
				success = true;
				System.out.println("Parsed as " + modelImporter);
				break;
			} catch (Exception ex) {
				System.err.println("Cannot parse as " + modelImporter + " due to: " + ex);
			}
		}

		if (!success)
			throw new IOException("Could not use any importer in MultiImporter");
	}

	private class MultiFileFilter implements FileFilter {
		@Override
		public boolean accept(File pathname) {
			for (ModelImporter modelImporter : importers) {
				if (modelImporter.getFileFilter().accept(pathname))
					return true;
			}
			return false;
		}

	}

}
