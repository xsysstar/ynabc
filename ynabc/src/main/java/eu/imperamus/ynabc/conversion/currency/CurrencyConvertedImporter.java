package eu.imperamus.ynabc.conversion.currency;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import eu.imperamus.ynabc.conversion.ModelImporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.SimpleAccount;
import eu.imperamus.ynabc.domain.Transaction;

@RequiredArgsConstructor
public class CurrencyConvertedImporter implements ModelImporter {
	private final @NonNull @Getter ModelImporter inner;
	private final @NonNull @Getter Currency targetCurrency;

	@Override
	public FileFilter getFileFilter() {
		return inner.getFileFilter();
	}

	@Override
	public void importModel(File file, Account model) throws IOException,
			ParseException {
		Account temp = new SimpleAccount("temporary");

		// Import into temp
		inner.importModel(file, temp);

		// Inspect conversions
		List<Transaction> transactions = new ArrayList<>(temp.getTransactions());

		Set<ConversionPair> conversions = getConversions(transactions);
		for (ConversionPair pair : conversions) {
			transactions.remove(pair.getForeignTransaction());
			transactions.remove(pair.getTargetTransaction());
		}

		// Convert
		for (Transaction transaction : transactions) {
			if (isForeignCurrencyTransaction(transaction)) {
				ConversionPair pair = getConversion(conversions, transaction);
				if (pair == null) {
					System.err.println("Could not convert transaction: "
							+ transaction);
					model.addTransaction(transaction);
					continue;
				}

				model.addTransaction(pair.convert(transaction));
			} else {
				model.addTransaction(transaction);
			}
		}
	}

	private ConversionPair getConversion(Set<ConversionPair> conversions,
			Transaction transaction) {
		for (ConversionPair pair : conversions) {
			Transaction ft = pair.getForeignTransaction();
			if (Objects.equals(ft.getDate(), transaction.getDate())
					&& Objects.equals(ft.getCurrency(),
							transaction.getCurrency())
					&& Objects.equals(ft.getFlow(), -transaction.getFlow())) {
				return pair;
			}
		}
		return null;
	}

	private Set<ConversionPair> getConversions(List<Transaction> transactions) {
		Set<ConversionPair> pairs = new HashSet<>();
		for (Transaction transaction : transactions) {
			if (isForeignCurrencyTransaction(transaction)) {
				Transaction foreignTransaction = transaction;

				// Get other half
				// FIXME: optimize: use Account.getTransactionsByDate(..)
				Transaction targetTransaction = getConversionPairTargetTransaction(
						transactions, foreignTransaction);

				if (targetTransaction != null) {
					pairs.add(new ConversionPair(targetTransaction,
							foreignTransaction));
				}
			}
		}
		return pairs;
	}

	private Transaction getConversionPairTargetTransaction(
			List<Transaction> transactions, Transaction foreignTransaction) {
		for (Transaction transaction : transactions) {
			if (Objects.equals(foreignTransaction.getMemo(),
					transaction.getMemo())
					&& Objects.equals(foreignTransaction.getDate(),
							transaction.getDate())
					&& Math.signum(foreignTransaction.getFlow()) == -Math
							.signum(transaction.getFlow())
					&& Objects
							.equals(transaction.getCurrency(), targetCurrency)) {
				return transaction;
			}
		}
		return null;
	}

	private boolean isForeignCurrencyTransaction(Transaction transaction) {
		return !transaction.getCurrency().equals(targetCurrency);
	}

	@Value
	class ConversionPair {
		public Transaction convert(Transaction transaction) {
			Transaction converted = new Transaction(transaction);
			converted.setMemo(getFlowAnnotatedMemo(transaction));
			converted.setFlow(Math.abs(targetTransaction.getFlow())
					* Math.signum(transaction.getFlow()));
			converted.setCurrency(targetTransaction.getCurrency());
			return converted;
		}

		private String getFlowAnnotatedMemo(Transaction transaction) {
			return transaction.getMemo() + " (= " + transaction.getCurrency()
					+ " " + transaction.getFlow() + ")";
		}

		private final @NonNull Transaction targetTransaction;
		private final @NonNull Transaction foreignTransaction;
	}
}
