package eu.imperamus.ynabc.conversion;

import java.io.IOException;
import java.io.OutputStream;

import eu.imperamus.ynabc.domain.Account;

public interface ModelExporter {
	public void exportModel(OutputStream toStream, Account model)
			throws IOException;
}
