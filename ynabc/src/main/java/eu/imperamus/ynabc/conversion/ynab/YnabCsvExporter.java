package eu.imperamus.ynabc.conversion.ynab;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import au.com.bytecode.opencsv.CSVWriter;
import eu.imperamus.ynabc.conversion.ModelExporter;
import eu.imperamus.ynabc.domain.Account;
import eu.imperamus.ynabc.domain.Transaction;

public class YnabCsvExporter extends YnabFormat implements ModelExporter {

	@Override
	public void exportModel(OutputStream toStream, Account model)
			throws IOException {
		try (CSVWriter writer = new CSVWriter(new OutputStreamWriter(toStream))) {
			writer.writeNext(HEADER);

			for (Transaction tr : model.getTransactions()) {
				String[] fields = new String[7];
				fields[0] = DATE_FORMAT.format(tr.getDate());
				fields[1] = tr.getCheckNumber();
				fields[2] = smartPayee(tr.getPayeeName(), tr.getPayeeNumber());
				fields[3] = tr.getCategory();
				fields[4] = tr.getMemo();
				fields[5] = tr.getFlow() > 0 ? "" : NUMBER_FORMAT.format(-tr
						.getFlow());
				fields[6] = tr.getFlow() < 0 ? "" : NUMBER_FORMAT.format(tr
						.getFlow());
				writer.writeNext(fields);
			}
		}
	}

}
