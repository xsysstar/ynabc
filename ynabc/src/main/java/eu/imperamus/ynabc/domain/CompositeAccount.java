package eu.imperamus.ynabc.domain;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CompositeAccount implements Account {

	protected final Map<String, Account> accounts;

	public Map<String, Account> getAccounts() {
		return Collections.unmodifiableMap(accounts);
	}

	@Override
	public List<Transaction> getTransactions() {
		List<Transaction> transactions = new LinkedList<Transaction>();
		for (Account account : getAccounts().values()) {
			for (Transaction transaction : account.getTransactions()) {
				transactions.add(transaction);
			}
		}
		Collections.sort(transactions);
		return Collections.unmodifiableList(transactions);
	}

	public String getAccountIdentifier(Transaction transaction) {
		String name = transaction.getAccountName() != null ? transaction
				.getAccountName() : transaction.getAccountNumber();
		return name + " " + transaction.getCurrency();
		// TODO: extract to Separator/grouper/categorizer/router
	}

	@Override
	public void addTransaction(Transaction transaction) {
		String accountIdentifier = getAccountIdentifier(transaction);
		Account account = accounts.get(accountIdentifier);
		if (account == null) {
			// No acceptable account found
			String identifier = accountIdentifier;
			account = new SimpleAccount(identifier);
			accounts.put(identifier, account);
		}
		account.addTransaction(transaction);
	}

	@Override
	public void removeTransaction(Transaction transaction) {
		for (Account account : accounts.values()) {
			account.removeTransaction(transaction);
		}
	}

}
