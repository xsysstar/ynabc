package eu.imperamus.ynabc.domain;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author Wanno Drijfhout
 * @see <a
 *      href="http://www.youneedabudget.com/support/article/csv-file-importing">YNAB
 *      reference</a>
 */
@Data
public class SimpleAccount implements Account {
	private final @NonNull String identifier;

	private List<Transaction> transactions = new LinkedList<Transaction>();

	@Override
	public List<Transaction> getTransactions() {
		Collections.sort(transactions);
		return Collections.unmodifiableList(transactions);
	}

	@Override
	public void addTransaction(Transaction transaction) {
		transactions.add(transaction);
	}

	@Override
	public void removeTransaction(Transaction transaction) {
		transactions.remove(transaction);
	}

	public double getBalance() {
		double sum = 0;
		for (Transaction transaction : getTransactions()) {
			sum += transaction.getFlow();
		}
		return sum;
	}
}
