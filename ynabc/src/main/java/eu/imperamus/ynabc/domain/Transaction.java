package eu.imperamus.ynabc.domain;

import java.util.Currency;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Transaction implements Comparable<Transaction> {
	private String accountNumber;
	private String accountName;
	private Date date;
	private String payeeNumber;
	private String payeeName;
	private String category;
	private String memo;
	private String checkNumber;
	/** Inflow is positive; outflow is negative */
	private double flow;
	private Currency currency;

	@Override
	public int compareTo(Transaction o) {
		return date.compareTo(o.date);
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Transaction(Transaction from) {
		this.accountNumber = from.accountNumber;
		this.accountName = from.accountName;
		this.date = from.date;
		this.payeeNumber = from.payeeNumber;
		this.payeeName = from.payeeName;
		this.category = from.category;
		this.memo = from.memo;
		this.checkNumber = from.checkNumber;
		this.flow = from.flow;
		this.currency = from.currency;
	}
}