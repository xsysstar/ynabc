package eu.imperamus.ynabc.domain;

import java.util.List;

public interface Account {

	public abstract List<Transaction> getTransactions();

	public abstract void addTransaction(Transaction transaction);

	public abstract void removeTransaction(Transaction transaction);

}